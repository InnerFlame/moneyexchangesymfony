<?php

namespace App\Repository;

use App\Entity\ExchangeRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class ExchangeRateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExchangeRate::class);
    }

    public function findOrThrowException(string $currencyFrom, string $currencyTo): ExchangeRate
    {
        # todo select more actual by date
        /** @var ExchangeRate $exchangeRate */
        $exchangeRate = $this->findOneBy([
            'currencyFrom' => $currencyFrom,
            'currencyTo'   => $currencyTo,
        ]);

        if (!$exchangeRate) {
            throw new \RuntimeException('ExchangeRate not fount.');
        }

        return $exchangeRate;
    }
}