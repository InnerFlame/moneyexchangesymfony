<?php

namespace App\Entity\Request;

use Symfony\Component\Validator\Constraints as Assert;

class ExchangeRateRequest
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=3, max=3)
     */
    protected $currency;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=3, max=3)
     */
    protected $currencyTarget;

    /**
     * @var int
     * @Assert\NotNull
     * @Assert\GreaterThan(0)
     */
    protected $amount;

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCurrencyTarget(): string
    {
        return $this->currencyTarget;
    }

    public function setCurrencyTarget(string $currencyTarget): self
    {
        $this->currencyTarget = $currencyTarget;

        return $this;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}