<?php

namespace App\Entity\Response;

use App\Entity\Request\ExchangeRateRequest;
use Symfony\Component\Validator\Constraints as Assert;

class ExchangeRateResponse extends ExchangeRateRequest
{
    /**
     * @var int
     * @Assert\NotNull()
     */
    private $amountTarget;

    public function getAmountTarget(): int
    {
        return $this->amountTarget;
    }

    public function setAmountTarget(int $amountTarget): self
    {
        $this->amountTarget = $amountTarget;

        return $this;
    }

    public function getData(): array
    {
        return [
            'currencyFrom' => $this->currency,
            'currencyTo'   => $this->currencyTarget,
            'amount'       => $this->amount,
            'amountTarget' => $this->amountTarget,
        ];
    }
}