<?php

namespace App\Service;

use App\Entity\Request\ExchangeRateRequest;
use App\Entity\Response\ExchangeRateResponse;
use App\Repository\ExchangeRateRepository;

final class ExchangeRateService
{
    /** @var ExchangeRateRepository */
    private $exchangeRateRepository;

    public function __construct(ExchangeRateRepository $exchangeRateRepository)
    {
        $this->exchangeRateRepository = $exchangeRateRepository;
    }

    public function exchange(ExchangeRateRequest $exchangeRateRequest): ExchangeRateResponse
    {
        $exchangeRate = $this->exchangeRateRepository->findOrThrowException(
            $exchangeRateRequest->getCurrency(),
            $exchangeRateRequest->getCurrencyTarget()
        );

        $exchangeRateResponse = (new ExchangeRateResponse())
            ->setCurrency($exchangeRateRequest->getCurrency())
            ->setCurrencyTarget($exchangeRateRequest->getCurrencyTarget())
            ->setAmount($exchangeRateRequest->getAmount())
            ->setAmountTarget($exchangeRateRequest->getAmount() * $exchangeRate->getRate())
        ;

        return $exchangeRateResponse;
    }
}