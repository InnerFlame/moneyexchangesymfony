<?php

namespace App\Controller\Rest;

use App\Entity\Request\ExchangeRateRequest;
use App\Entity\Response\ExchangeRateResponse;
use App\Service\ExchangeRateService;
use FOS\RestBundle\Controller\Annotations;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CurrencyController
{
    /** @var ExchangeRateService */
    private $exchangeRateService;

    /** @var $logger LoggerInterface */
    private $logger;

    public function __construct(
        ExchangeRateService $exchangeRateService,
        LoggerInterface $logger
    ) {
        $this->exchangeRateService = $exchangeRateService;
        $this->logger = $logger;
    }

    /**
     * @Annotations\Route(
     *   "currency/{currency}/target/{currencyTarget}/amount/{amount}/exchange",
     *   methods={"GET"}
     * )
     */
    public function exchange(Request $request): Response
    {
        /** @var ExchangeRateRequest $exchangeRateRequest */
        $exchangeRateRequest = (new ExchangeRateRequest())
            ->setCurrency($request->attributes->get('currency'))
            ->setCurrencyTarget($request->attributes->get('currencyTarget'))
            ->setAmount($request->attributes->get('amount'))
        ;

        try {
            /** @var ExchangeRateResponse $exchangeRateResponse */
            $exchangeRateResponse = $this->exchangeRateService->exchange($exchangeRateRequest);
            /** @var Response $response */
            $response = new Response(json_encode($exchangeRateResponse->getData()), Response::HTTP_OK);

            return $response;
        } catch (\Exception $e) {
            $this->logger->critical(
                'Catch api RequestException!',
                [
                    'currencyFrom' => $exchangeRateRequest->getCurrency(),
                    'currencyTo'   => $exchangeRateRequest->getCurrencyTarget(),
                    'amount'       => $exchangeRateRequest->getAmount(),
                ]
            );

            return new Response('', Response::HTTP_BAD_REQUEST);
        }
    }

}