<?php

namespace App\Component\ExchangeRates;

use App\Entity\ExchangeRate;
use SimpleXMLElement;

class ClientEcb extends ClientHttp
{
    public function getUrl(): string
    {
        return 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml';
    }

    /**
     * @return ExchangeRate[]
     */
    public function parse(): array
    {
        $exchangeRates = [];
        $xml = new SimpleXMLElement(file_get_contents($this->getUrl()));

        foreach($xml->Cube->Cube->Cube as $node){

            $exchangeRate = new ExchangeRate();
            $exchangeRate
                ->setCurrencyFrom('EUR')
                ->setCurrencyTo($node['currency'])
                ->setRate($node["rate"])
            ;

            $exchangeRates[] = $exchangeRate;
        }

        return $exchangeRates;
    }
}