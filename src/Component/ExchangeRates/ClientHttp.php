<?php

namespace App\Component\ExchangeRates;

use App\Entity\ExchangeRate;

abstract class ClientHttp
{
    abstract public function getUrl(): string;

    /** @return ExchangeRate[] */
    abstract public function parse(): array;
}