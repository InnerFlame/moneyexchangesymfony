<?php

namespace App\Component\ExchangeRates;

use App\Entity\ExchangeRate;
use SimpleXMLElement;

class ClientCbr extends ClientHttp
{
    public function getUrl(): string
    {
        return 'https://www.cbr.ru/scripts/XML_daily.asp';
    }

    /**
     * @return ExchangeRate[]
     */
    public function parse(): array
    {
        $exchangeRates = [];

        foreach(new SimpleXMLElement(file_get_contents($this->getUrl())) as $node){
            $exchangeRate = new ExchangeRate();

            $exchangeRate
                ->setCurrencyFrom('RUB')
                ->setCurrencyTo($node->CharCode[0])
                ->setRate($node->Value[0])
            ;

            $exchangeRates[] = $exchangeRate;
        }

        return $exchangeRates;
    }
}