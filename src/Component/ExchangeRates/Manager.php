<?php

namespace App\Component\ExchangeRates;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Yaml\Yaml;

class Manager
{
    const DEFAULT_SERVICE_CLIENT = 'app.exchange_rates_ecb_client';

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var string */
    private $exchangeRatesServices;

    /** @var AbstractClient */
    private $clientEcb;

    /** @var AbstractClient */
    private $clientCbr;

    public function __construct(
        EntityManagerInterface $entityManager,
        string $exchangeRatesServices,
        ClientEcb $clientEcb,
        ClientCbr $clientCbr
    ) {
        $this->entityManager = $entityManager;
        $this->exchangeRatesServices = $exchangeRatesServices;
        $this->clientEcb = $clientEcb;
        $this->clientCbr = $clientCbr;
    }

    public function import()
    {
        $exchangeRatesClientName = $this->getClientName();
        /** @var ClientHttp $exchangeRatesClient */
        $exchangeRatesClient = $this->getClient($exchangeRatesClientName);
        /** @var ExchangeRate[] $exchangeRates */
        $exchangeRates = $exchangeRatesClient->parse();

        foreach($exchangeRates as $exchangeRate) {
            $this->entityManager->persist($exchangeRate);
        }

        $this->entityManager->flush();
    }

    private function getClientName(): string
    {
        $services = Yaml::parse(file_get_contents($this->exchangeRatesServices));

        foreach ($services as $service) {
            if (isset($service['enableService'], $service['client'])
                && $service['enableService']
                && $service['client']
            ) {
                return $service['client'];
            }
        }

        return self::DEFAULT_SERVICE_CLIENT;
    }

    private function getClient(string $clientName): ClientHttp
    {
        if (!property_exists($this, $clientName)) {
            // todo throw exception;
        }

        return $this->$clientName;
    }
}