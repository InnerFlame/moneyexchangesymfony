<?php

namespace App\Command;

use App\Component\ExchangeRates\Manager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportExchangeRatesCommand extends Command
{
    /** @var Manager */
    private $exchangeRatesManager;

    protected static $defaultName = 'app:import-exchange-rates';

    public function __construct(Manager $exchangeRatesManager)
    {
        $this->exchangeRatesManager = $exchangeRatesManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Import exchange rates from service')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $this->exchangeRatesManager->import();

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');
    }
}
